<?php
/*
 * Local configuration file to provide any overrides to your app.php configuration.
 * Copy and save this file as app_local.php and make changes as required.
 * Note: It is not recommended to commit files with credentials such as app_local.php
 * into source code version control.
 */
return [
    /*
     * Debug Level:
     *
     * Production Mode:
     * false: No error messages, errors, or warnings shown.
     *
     * Development Mode:
     * true: Errors and warnings shown.
     */
    'debug' => filter_var(env('DEBUG', true), FILTER_VALIDATE_BOOLEAN),

    /*
     * Security and encryption configuration
     *
     * - salt - A random string used in security hashing methods.
     *   The salt value is also used as the encryption key.
     *   You should treat it as extremely sensitive data.
     */
    'Security' => [
        'salt' => env('SECURITY_SALT', '58dfef671e7fa4faa4ffa808d0a0e02ccd75f459570e6aff96898cb8754bb5d5'),
    ],

    /*
     * Connection information used by the ORM to connect
     * to your application's datastores.
     *
     * See app.php for more configuration options.
     */
    'Datasources' => [
        'default' => [
            'host' => 'localhost',
            /*
             * CakePHP will use the default DB port based on the driver selected
             * MySQL on MAMP uses port 8889, MAMP users will want to uncomment
             * the following line and set the port accordingly
             */
            //'port' => 'non_standard_port_number',

            'username' => 'root',
            'password' => '',
            'database' => 'sakila',
            /**
             * If not using the default 'public' schema with the PostgreSQL driver
             * set it here.
             */
            //'schema' => 'myapp',

            /**
             * You can use a DSN string to set the entire configuration
             */
            'url' => env('DATABASE_URL', null),
        ],

        /*
         * The test connection is used during the test suite.
         */
        'test' => [
            'host' => 'localhost',
            //'port' => 'non_standard_port_number',
            'username' => 'my_app',
            'password' => 'secret',
            'database' => 'test_myapp',
            //'schema' => 'myapp',
        ],
    ],

    /*
     * Email configuration.
     *
     * Host and credential configuration in case you are using SmtpTransport
     *
     * See app.php for more configuration options.
     */
    'EmailTransport' => [
        'default' => [
            'className' => 'Smtp',
            'host' => 'ssl://chetaa.com',
            'port' => 465,
            'timeout' => 500,
            'username' => 'hello@chetaa.com',
            'password' => 'Omopeyemi@2007',
            'client' => null,
            'url' => env('EMAIL_TRANSPORT_DEFAULT_URL', null),
        ],
        'custom' => [
            'className' => 'Smtp',
            // The following keys are used in SMTP transports
            'host' => '68.65.121.175', // Ip of your server
            'port' => 21098, // Port that you use: 25 or 465 or 587
            'timeout' => 60,
            'username' => 'hello@chetaa.com',
            'password' => 'Omopeyemi@2007', 

            'client' => null,
            'tls' => true,
            'url' => env('EMAIL_TRANSPORT_DEFAULT_URL', null),
        ],
        'gmail' => [
            'className' => 'Smtp',
            // The following keys are used in SMTP transports
            //'host' => 'ssl://chetaa.com',
            'host' => 'ssl://smtp.gmail.com',
            'port' => 465,
            'timeout' => 500,
            'username' => 'mojeed.oyedeji@gmail.com',
            'password' => 'mojisolaaduke2020',
            'client' => null,
            'tls' => null,
            'url' => env('EMAIL_TRANSPORT_DEFAULT_URL', null),
            
        ],
    ],
    'Email' => [
        'default' => [
            'transport' => 'default',
            'from' => 'hello@chetaa.com',
            //'charset' => 'utf-8',
            //'headerCharset' => 'utf-8',
        ],
        'gmail' => [
            'transport' => 'gmail',
            'from' => 'hello@chetaa.com',
            //'charset' => 'utf-8',
            //'headerCharset' => 'utf-8',
        ],

        'custom' => [
            'transport' => 'custom',
            'from' => 'hello@chetaa.com',
            //'charset' => 'utf-8',
            //'headerCharset' => 'utf-8',
        ],
     ],
];
