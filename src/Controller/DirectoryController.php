<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\EventInterface;
use App\Api;


/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/4/en/controllers.html#the-app-controller
 */
class DirectoryController extends Controller
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('FormProtection');`
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
    }

    

    public function index(){
      $this->RequestHandler->renderAs($this, 'json'); //render as json
      $message = 'Chetaa API v1.0';
      $this->set([
          'message' => $message,
          '_serialize' => ['message']
      ]);
    }

    public function beforeFilter(EventInterface $event)
    {
      $this->loadComponent('RequestHandler');
      $this->loadComponent('Flash');
      if ($this->request->is('options')) {
          return $this->response;
      }
    }

    public function slugs(){
      $this->RequestHandler->renderAs($this, 'json'); //render as json
      $response = []; $message = "ok"; $status = "success";
      $data = $this->request->input('json_decode', true );
      $params = $this->request->getParam('pass'); //gets passed param
      $action = $params[0];
      $directory = new Api\Directory();
      try{
        switch($action){
          case 'generate':
            $response["directory"] = $directory->generateSlugs();
            break;
        }
      }catch(Exception $e){
          $message = $e->getMessage();
      }
      $this->set([
          'response' => $response,
          'message' => $message,
          'status' => $status,
          '_serialize' => ['response','message', 'status']
      ]);
  }



   

    
    public function business(){
      $this->RequestHandler->renderAs($this, 'json'); //render as json
      $response = []; $message = "ok"; $status = "success";
      $data = $this->request->input('json_decode', true );
      $params = $this->request->getParam('pass'); //gets passed param
      $action = $params[0];
      $directory = new Api\Directory();
      try{
        switch($action){
          case 'add':
            $response = $directory->addBusiness($data);
            if($response == false){
              $message = "An error occured";
              $status = "error";
            }else if($response == null){
              $message = "Business already exists";
              $status = "error";
            }else{
              $response['directory'] = $response;
            }
            break;
          case 'find':
            $response["business"] = $directory->find($data);
            break;
          case 'all':
            $response["directory"] = $directory->all();
            break;
          case 'update':
            $response["directory"] = $directory->update($data);
            break;
          case 'authentication':
            $response["business"] = $directory->authentication(strval($data["id"]));
            break;
          case 'verify':
            $response["business"] = $directory->verify($data);
            break;
          case 'remove':
            $response['directory'] = $directory->remove($data);
            break;
        }
      }catch(Exception $e){
          $message = $e->getMessage();
      }
      $this->set([
          'response' => $response,
          'message' => $message,
          'status' => $status,
          '_serialize' => ['response','message', 'status']
      ]);
  }


    



}
