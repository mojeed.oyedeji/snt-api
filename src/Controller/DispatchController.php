<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\EventInterface;
use App\Api;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/4/en/controllers.html#the-app-controller
 */
class DispatchController extends Controller
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('FormProtection');`
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Basic' => [
                    'fields' => ['username' => 'email',
                    'password' => 'password'],
                    'userModel' => 'Admin'
                ],
            ],
            'storage' => 'Memory',
            'finder' => 'auth',
            'unauthorizedRedirect' => false,
        ]);
        $this->Auth->allow(['find', 'register', 'images', 'avatar',
        'confirm', 'forgot', 'reset']);

    }

    public function beforeFilter(EventInterface $event)
    {
      if ($this->request->is('options')) {
          return $this->response;
      }
    }

    public function index(){
      $this->RequestHandler->renderAs($this, 'json'); //render as json
      $message = 'Chetaa API v2.0';
      $this->set([
          'message' => $message,
          '_serialize' => ['message']
      ]);
    }

    

    public function find(){
      $this->RequestHandler->renderAs($this, 'json'); //render as json
      $response = []; $message = "";
      $data = $this->request->input('json_decode', true );
      $dispatch = new Api\Dispatch();
      try{
        if($data != null){
          $response["dispatch"] = $dispatch->findDispatch($data['phone']);
          $message = 'success';
        }else{
          $message = "no data";
        }
      }catch(Exception $e){
          $message = $e->getMessage();
      }
      $this->set([
          'response' => $response,
          'message' => $message,
          '_serialize' => ['response','message']
      ]);
     }



    public function delivery(){
        $this->RequestHandler->renderAs($this, 'json'); //render as json
        $response = []; $message = "ok"; $status = "success";
        $data = $this->request->input('json_decode', true );
        $params = $this->request->getParam('pass'); //gets passed param
        $action = $params[0];
        $delivery = new Api\Delivery();
        $deliveryLogs = new Api\DeliveryLog();
        try{
          switch($action){
            case 'all':
              $response["delivery"] = $delivery->agent($data["agent"]);
              break;
            case 'get':
              $response["delivery"] = $delivery->get($data['id']);
              break;
            case 'accept':
              $response['delivery'] = $delivery->assign($data);
              $response['logs'] = $deliveryLogs->request($data['delivery']);
              break;
            case 'status':
              $response['delivery'] = $delivery->status($data, 'dispatch');
              $response['logs'] = $deliveryLogs->request($data['delivery']);
              break;
            case 'logs':
              $response['logs'] = $deliveryLogs->request($data['delivery']);
              break;
            case 'start':
              $response['delivery'] = $delivery->start($data);
              $response['logs'] = $deliveryLogs->request($data['delivery']);
              break;
            case 'complete':
              $response['delivery'] = $delivery->complete($data);
              $response['logs'] = $deliveryLogs->request($data['delivery']);
              break;
          }
        }catch(Exception $e){
            $message = $e->getMessage();
        }
        $this->set([
            'response' => $response,
            'message' => $message,
            'status' => $status,
            '_serialize' => ['response','message', 'status']
        ]);
    }

    public function notifications(){
      $this->RequestHandler->renderAs($this, 'json'); //render as json
      $response = []; $message = "ok"; $status = "success";
      $data = $this->request->input('json_decode', true );
      $params = $this->request->getParam('pass'); //gets passed param
      $action = $params[0];
      $notifications = new Api\Notification();
      try{
        switch($action){
          case 'all':
            $response["notifications"] = $notifications->recipient($data["dispatch"]);
            break;
        }
      }catch(Exception $e){
          $message = $e->getMessage();
      }
      $this->set([
          'response' => $response,
          'message' => $message,
          'status' => $status,
          '_serialize' => ['response','message', 'status']
      ]);
  }

  public function agent(){
    $this->RequestHandler->renderAs($this, 'json'); //render as json
    $response = []; $message = "ok"; $status = "success";
    $data = $this->request->input('json_decode', true );
    $params = $this->request->getParam('pass'); //gets passed param
    $action = $params[0];
    $agent = new Api\Agent();
    try{
      switch($action){
        case 'get':
          $response["agent"] = $agent->get($data["id"]);
          break;
      }
    }catch(Exception $e){
        $message = $e->getMessage();
    }
    $this->set([
        'response' => $response,
        'message' => $message,
        'status' => $status,
        '_serialize' => ['response','message', 'status']
    ]);
}


    public function profile(){
        $this->RequestHandler->renderAs($this, 'json'); //render as json
        $response = []; $message = "ok"; $status = "success";
        $data = $this->request->input('json_decode', true );
        $params = $this->request->getParam('pass'); //gets passed param
        $action = $params[0];
        $dispatch = new Api\Dispatch();
        try{
            switch ($action) {
              case 'create':
                  $response['dispatch'] = $dispatch->createProfile($data);
                break;
              case 'get':
                if($data != null){
                  $response['dispatch'] = $dispatch->get($data["id"]);
                }else{
                  $message = 'no data';
                }
                break;
              case 'update':
                if($data != null){
                  $response['dispatch'] = $dispatch->update($data);
                }else{
                  $message = 'no data';
                }
              break;
              default:
                $response = [];
                $message = "no action";
                $status = "warning";
            }
        }catch(Exception $e){
            $message = $e->getMessage();
            $status = "error";
        }
        $this->set([
            'response' => $response,
            'message' => $message,
            'status' => $status,
            '_serialize' => ['response','message', 'status']
        ]);
    }

    public function images(){
      $this->RequestHandler->renderAs($this, 'json'); //render as json
      $response = []; $message = "ok"; $status = "success";
      $data = $this->request->getParsedBody();
      $params = $this->request->getParam('pass'); //gets passed param
      $action = $params[0];
      $images = new Api\Images();
      try{
        switch($action){
          case 'upload':
            $result = $images->upload($data);
            if($result == -1){
              $message = "file is too large";
              $status = "error";
            }else if($result == -2){
              $message = "unacceptable file type";
              $status = "error";
            }else{
              $response["image"] = $result;
            }
            break;
          case 'fetch':
              $data = $this->request->input('json_decode', true );
              if($data != null){
                $response = $images->avatar($data['uri']);
                if($response != null){
                  $message = 'success';
                }else{
                  $message = 'not found';
                }
              }
            return $response;
          case 'find':
            if($data != null){
              $response = $images->get($data);
              if($response != null){
                $message = 'success';
              }else{
                $message = 'not found';
              }
            }
          case 'delete':
            $response = $images->all();
            break;
          case 'replace':
            $response["image"] = $add->replace($data);
            break;
          case 'get':
            $response = $add->get($data['id']);
            break;
          default:
            $response = [];
            $message = "no action";
        }
      }catch(Exception $e){
          $message = $e->getMessage();
      }
      $this->set([
          'response' => $response,
          'message' => $message,
          'status' => $status,
          '_serialize' => ['response','message', 'status']
      ]);
    }

    public function avatar(){
      $this->RequestHandler->renderAs($this, 'json'); //render as json
      $response = []; $message = "ok"; $status = "success";
      $data = $this->request->getParsedBody();
      $params = $this->request->getParam('pass'); //gets passed param
      $uri = $params[0];
      $images = new Api\Images();

        if($uri != null){
          $response = $images->avatar($uri);
        }
      return $response;
    }
}




