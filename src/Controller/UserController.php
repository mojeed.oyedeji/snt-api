<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\EventInterface;
use App\Api;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/4/en/controllers.html#the-app-controller
 */
class UserController extends Controller
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('FormProtection');`
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
    }

    

    public function index(){
      $this->RequestHandler->renderAs($this, 'json'); //render as json
      $message = 'Chetaa API v1.0';
      $this->set([
          'message' => $message,
          '_serialize' => ['message']
      ]);
    }

    public function beforeFilter(EventInterface $event)
    {
      $this->loadComponent('RequestHandler');
      $this->loadComponent('Flash');
      if ($this->request->is('options')) {
          return $this->response;
      }
    }

    public function find(){
        $this->RequestHandler->renderAs($this, 'json'); //render as json
        $response = []; $message = "";
        $data = $this->request->input('json_decode', true );
        $user = new Api\Users();
        try{
          if($data != null){
            $response["user"] = $user->findUser($data['phone']);
            $message = 'success';
          }else{
            $message = "no data";
          }
        }catch(Exception $e){
            $message = $e->getMessage();
        }
        $this->set([
            'response' => $response,
            'message' => $message,
            '_serialize' => ['response','message']
        ]);
    }

    public function register(){
      $this->RequestHandler->renderAs($this, 'json'); //render as json
      $response = []; $message = "";
      $data = $this->request->input('json_decode', true );
      $user = new Api\Users();
      try{
        if($data != null){
          $result = $user->register($data);
          if($result == false){
            $message = "phone or email already exists";
            $status = "error";
          }else if ($result == null){
            $message = "unknown error";
            $status = "error";
          }else{
            $response["user"] = $result;
            $message = "success";
            $status = "success";
          }
        }else{
          $message = "no data";
        }
      }catch(Exception $e){
          $message = $e->getMessage();
      }
      $this->set([
          'response' => $response,
          'message' => $message,
          'status' => $status,
          '_serialize' => ['response','message', 'status']
      ]);
    }

    public function customers(){
      $this->RequestHandler->renderAs($this, 'json'); //render as json
      $response = []; $message = "ok"; $status = "success";
      $data = $this->request->input('json_decode', true );
      $params = $this->request->getParam('pass'); //gets passed param
      $action = $params[0];
      $customers = new Api\Customers();
      try{
        switch($action){
          case 'add':
            $response = $customers->add($data);
            if($response == 0){
              $message = "An error occured";
              $status = "error";
            }else if($response == -1){
              $message = "Customer already exists";
              $status = "error";
            }else{
              $response['customers'] = $response;
            }
            break;
          case 'all':
            $response["customers"] = $customers->client($data["client"]);
            break;
          case 'get':
            $response = $customers->get($data['id']);
            break;
          case 'update':
            $response["customers"] = $customers->update($data);
            break;
          case 'remove':
            $response['customers'] = $customers->remove($data);
            break;
        }
      }catch(Exception $e){
          $message = $e->getMessage();
      }
      $this->set([
          'response' => $response,
          'message' => $message,
          'status' => $status,
          '_serialize' => ['response','message', 'status']
      ]);
  }


    
  public function issues(){
    $this->RequestHandler->renderAs($this, 'json'); //render as json
    $response = []; $message = "ok"; $status = "success";
    $data = $this->request->input('json_decode', true );
    $params = $this->request->getParam('pass'); //gets passed param
    $action = $params[0];
    $issues = new Api\Issue();
    $issueLog = new Api\IssueLog();
    try{
      switch($action){
        case 'all':
          $response["issues"] = $issues->client($data["client"]);
          break;
        case 'get':
          $response = $issues->get($data['id']);
          break;
        case 'create':
          $result = $issues->create($data);
          if($result == 0){
              $message = "An error occured";
              $status = "error";
          }else{
              $response["issues"] = $result;
          }
          break;
        case 'resolve':
          $response['issue'] = $issues->resolve($data);
          break;
        case 'log':
          $logs = $issueLog->create($data);
          $response["issue"] = $issues->get($data["issue"]);
          break;
        default:
          $response = [];
          $message = "no action";
      }
    }catch(Exception $e){
        $message = $e->getMessage();
    }
    $this->set([
        'response' => $response,
        'message' => $message,
        'status' => $status,
        '_serialize' => ['response','message', 'status']
    ]);
  }

  public function profile(){
    $this->RequestHandler->renderAs($this, 'json'); //render as json
    $response = []; $message = "ok"; $status = "success";
    $data = $this->request->input('json_decode', true );
    $params = $this->request->getParam('pass'); //gets passed param
    $action = $params[0];
    $users = new Api\Users();

    try{
      switch($action){
        case 'update':
          $response["user"] = $users->update($data);
          break;
        case 'get':
          $response = $users->get($data['id']);
          break;
        default:
          $response = [];
          $message = "no action";
      }
    }catch(Exception $e){
        $message = $e->getMessage();
    }
    $this->set([
        'response' => $response,
        'message' => $message,
        'status' => $status,
        '_serialize' => ['response','message', 'status']
    ]);
  }

    public function delivery(){
      $this->RequestHandler->renderAs($this, 'json'); //render as json
      $response = []; $message = "ok"; $status = "success";
      $data = $this->request->input('json_decode', true );
      $params = $this->request->getParam('pass'); //gets passed param
      $action = $params[0];
      $delivery = new Api\Delivery();

      try{
        switch($action){
          case 'new':
            $response["delivery"] = $delivery->new_app_request($data);
            break;
          case 'get':
            $response = $delivery->get($data['id']);
            break;
          case 'requests':
            $response["delivery"] = $delivery->user($data['user']);
            break;
          default:
            $response = [];
            $message = "no action";
        }
      }catch(Exception $e){
          $message = $e->getMessage();
      }
      $this->set([
          'response' => $response,
          'message' => $message,
          'status' => $status,
          '_serialize' => ['response','message', 'status']
      ]);
    }


    public function transactions(){

    }

    public function notifications(){

    }
    public function images(){
        
    }



}
