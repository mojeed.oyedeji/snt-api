<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\EventInterface;
use App\Api;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/4/en/controllers.html#the-app-controller
 */
class CollectionController extends Controller
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('FormProtection');`
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Basic' => [
                    'fields' => ['username' => 'email',
                    'password' => 'password'],
                    'userModel' => 'Account'
                ],
            ],
            'storage' => 'Memory',
            'finder' => 'auth',
            'unauthorizedRedirect' => false,
        ]);
        $this->Auth->allow(['address']);

    }

    public function beforeFilter(EventInterface $event)
    {
      $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        if ($this->request->is('options')) {
            return $this->response;
        }
    }

    public function index(){
      $this->RequestHandler->renderAs($this, 'json'); //render as json
      $message = 'RoofReno API v1.0 | Partner';
      $this->set([
          'message' => $message,
          '_serialize' => ['message']
      ]);
    }



    public function address(){
        $this->RequestHandler->renderAs($this, 'json'); //render as json
        $response = []; $message = "ok"; $status = "success";
        $data = $this->request->input('json_decode', true );
        $params = $this->request->getParam('pass'); //gets passed param
        $action = $params[0];
        $add = new Api\Address();
        try{
          switch($action){
            case 'create':
              $result = $add->create($data);
              if($result == 0){
                $message = "An error occured";
                $status = "error";
              }else if($result == -1){
                $message = "address already exists";
                $status = "error";
              }else{
                $response["address"] = $result;
              }
              break;
            case 'complete':
              $response['address'] = $add->complete($data);
              break;
            case 'collection':
              $response["address"] = $add->collection($data['collection_id']);
              break;
            case 'update':
              $response["address"] = $add->update($data);
              break;
            case 'get':
              $response = $add->get($data['id']);
              break;
            default:
              $response = [];
              $message = "no action";
          }
        }catch(Exception $e){
            $message = $e->getMessage();
        }
        $this->set([
            'response' => $response,
            'message' => $message,
            'status' => $status,
            '_serialize' => ['response','message', 'status']
        ]);
    }

    public function collection(){
      $this->RequestHandler->renderAs($this, 'json'); //render as json
      $response = []; $message = "ok"; $status = "success";
      $data = $this->request->input('json_decode', true );
      $params = $this->request->getParam('pass'); //gets passed param
      $action = $params[0];
      $collection = new Api\Collections();
      try{
        switch($action){
          case 'all':
            $response['collection'] = $collection->partner($data['id']);
            break;
          case 'get':
            $response['collection'] = $collection->get($data['id']);
            break;
        }
      }catch(Exception $e){
          $message = $e->getMessage();
      }
      $this->set([
          'response' => $response,
          'message' => $message,
          'status' => $status,
          '_serialize' => ['response','message', 'status']
      ]);
  }


    
    public function login(){
      $this->RequestHandler->renderAs($this, 'json'); //render as json
      $response = []; $message = "ok"; $status = "success";
      $data = $this->request->input('json_decode', true );
      $partner = new Api\Partner();
      try{
        if($data != null){
          $response['partner'] = $partner->doLogin($data);
          //$response = [];
        }else{
          $message = 'no data';
        }
      }catch(Exception $e){
          $message = $e->getMessage();
          $status = "error";
      }
      $this->set([
          'response' => $response,
          'message' => $message,
          'status' => $status,
          '_serialize' => ['response','message', 'status']
      ]);
    }

    public function credentials(){
      $this->RequestHandler->renderAs($this, 'json'); //render as json
      $response = []; $message = ""; $status = "success";
      $data = $this->request->input('json_decode', true );
      $params = $this->request->getParam('pass'); //gets passed param
      $action = $params[0];
      $partners = new Api\Partner();

      try{
        switch($action){
            case 'create':
              $partner = $partners->setPassword($data);
                if($partner == 0){
                  $message = "account not found";
                  $status = "error";
                }else{
                  $response["partner"] = $partner;
                }
              break;  
            default:
               //0 $response = [];
               // $message = "no action";
        }
      }catch(Exception $e){
          $message = $e->getMessage();
      }
      $this->set([
          'response' => $response,
          'message' => $message,
          'status' => $status,
          '_serialize' => ['response','message', 'status']
      ]);
  }

  public function measure(){
    $this->RequestHandler->renderAs($this, 'json'); //render as json
    $response = []; $message = ""; $status = "success";
    $data = $this->request->input('json_decode', true );
    $params = $this->request->getParam('pass'); //gets passed param
    $action = $params[0];
    $measure = new Api\Measure();

    try{
      switch($action){
          case 'create':
            $measures = $measure->create($data);
            $response['measurement'] = $measures;
            break;
          case 'update':
            $measures = $measure->update($data);
            $response['measurement'] = $measures;
            break;
          case 'address':
            $measures = $measure->address($data['address_id']);
            $response['measurement'] = $measures;
            break;
          case 'delete':
            $measures = $measure->delete($data);
            $response['measurement'] = $measures;
            break;
          default:
             //0 $response = [];
             // $message = "no action";
      }
    }catch(Exception $e){
        $message = $e->getMessage();
    }
    $this->set([
        'response' => $response,
        'message' => $message,
        'status' => $status,
        '_serialize' => ['response','message', 'status']
    ]);
  }
}
