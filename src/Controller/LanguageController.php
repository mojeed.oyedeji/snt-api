<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\EventInterface;
use App\Api;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/4/en/controllers.html#the-app-controller
 */
class LanguageController extends Controller
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('FormProtection');`
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
    }

    public function beforeFilter(EventInterface $event)
    {
      if ($this->request->is('options')) {
          return $this->response;
      }
    }

    public function all(){
      $this->RequestHandler->renderAs($this, 'json'); //render as json
      $response = []; $message = "ok"; $status = "success";
      $data = $this->request->input('json_decode', true );
      $language = new Api\Language();
      try{
        $response['languages'] = $language->all();
      }catch(Exception $e){
          $message = $e->getMessage();
          $status = "error";
      }
      $this->set([
          'response' => $response,
          'message' => $message,
          'status' => $status,
          '_serialize' => ['response','message', 'status']
      ]);
    }

    public function active(){

    }

    public function inactive(){

    }





    public function create(){
      $this->RequestHandler->renderAs($this, 'json'); //render as json
      $response = []; $message = "ok"; $status = "success";
      $data = $this->request->getParsedBody();
      $params = $this->request->getParam('pass'); //gets passed param
      $action = $params[0];
      $customer = new Api\Customer();
      $address = new Api\Address();
      try{
        switch($action){
          case 'new':
            $result = $customer->newCustomer($data);
            if($result == -1){
              $message = "customer already exists";
              $status = "error";
            }else{
              $response['customer'] = $result; 
              $response['address'] = $address->customer($result[0]['id']);
            }
            break;
          case 'existing':
            $result = $customer->existCustomer($data);
            $response['customer'] = $result; 
            if($result == -1){
              $message = "customer already exists";
              $status = "error";
            }else{
              $response['customer'] = $result; 
            }
            break;
          default:
            $response = [];
            $message = "no action";
        }
      }catch(Exception $e){
          $message = $e->getMessage();
      }
      $this->set([
          'response' => $response,
          'message' => $message,
          'status' => $status,
          '_serialize' => ['response','message', 'status']
      ]);
    }

    public function login(){
      $this->RequestHandler->renderAs($this, 'json'); //render as json
      $response = []; $message = "ok"; $status = "success";
      $data = $this->request->input('json_decode', true );
      $customers = new Api\Customer();
      try{
        if($data != null){
          $response['user'] = $customers->doLogin($data);
        }else{
          $message = 'no data';
        }
      }catch(Exception $e){
          $message = $e->getMessage();
          $status = "error";
      }
      $this->set([
          'response' => $response,
          'message' => $message,
          'status' => $status,
          '_serialize' => ['response','message', 'status']
      ]);
    }

    public function quotes(){
      $this->RequestHandler->renderAs($this, 'json'); //render as json
      $response = []; $message = "ok"; $status = "success";
      $data = $this->request->getParsedBody();
      $params = $this->request->getParam('pass'); //gets passed param
      $action = $params[0];
      $quotes = new Api\Quotes();
      try{
        switch($action){
          case 'all':
            $response['quotes'] = $quotes->customer($data);
            break;
          case 'get':
            $result = $quotes->get($data);
            break;
          default:
            $response = [];
            $message = "no action";
        }
      }catch(Exception $e){
          $message = $e->getMessage();
      }
      $this->set([
          'response' => $response,
          'message' => $message,
          'status' => $status,
          '_serialize' => ['response','message', 'status']
      ]);
    }

    public function package(){
      $this->RequestHandler->renderAs($this, 'json'); //render as json
      $response = []; $message = "ok"; $status = "success";
      $data = $this->request->getParsedBody();
      $params = $this->request->getParam('pass'); //gets passed param
      $action = $params[0];
      $packages = new Api\Package();
      $materials = new Api\MaterialPackage();
      
      try{
        switch($action){
          case 'get':
            $response['packages'] = $packages->get($data['id']);
            $response['materials'] = $materials->package($data['id']);
            break;
          case 'all':
            $response['packages'] = $packages->all();
            break;
          default:
            $response = [];
            $message = "no action";
        }
      }catch(Exception $e){
          $message = $e->getMessage();
      }
      $this->set([
          'response' => $response,
          'message' => $message,
          'status' => $status,
          '_serialize' => ['response','message', 'status']
      ]);
    }

   



}
