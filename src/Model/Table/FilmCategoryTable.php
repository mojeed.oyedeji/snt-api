<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class FilmCategoryTable extends Table
{
    public function initialize(array $config): void
    {
        $this->setTable('film_category');
        $this->setPrimaryKey('id');
    }





    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */

}
