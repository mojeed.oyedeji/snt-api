<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class InventoryTable extends Table
{
    public function initialize(array $config): void
    {
        $this->setTable('inventory');
        $this->setPrimaryKey('id');
    }





    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */

}
