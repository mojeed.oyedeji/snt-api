<?php
namespace App\Api;

use Cake\Controller\Controller;
use App\Controller\UsersController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\Datasource\EntityInterface;
use Cake\Core\Configure;




Class Payment extends Controller{


   //Fetch all data
   public function all(){
    $table = TableRegistry::getTableLocator()->get('payment');
    $query = $table
            ->find();
    $result = $query->toArray();

    $response = $result;
    return $response;
  }

  public function customer($id){
    $table = TableRegistry::getTableLocator()->get('payment');
    $query = $table
            ->find()
            ->where(['customer_id' => $id]);
    $result = $query->toArray();
    return $result;
  }




}
