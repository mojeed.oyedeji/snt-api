<?php
namespace App\Api;

use Cake\Controller\Controller;
use App\Controller\UsersController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\Datasource\EntityInterface;
use Cake\Core\Configure;




Class FilmCategory extends Controller{

   public function getCategory($film){
    $table = TableRegistry::getTableLocator()->get('filmcategory');
    $query = $table
            ->find()
            ->where(['film_id' => $film]);
    $result = $query->toArray();
    
    return $result;
  }




}
