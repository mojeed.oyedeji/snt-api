<?php
namespace App\Api;

use Cake\Controller\Controller;
use App\Controller\UsersController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\Datasource\EntityInterface;
use Cake\Core\Configure;




Class Film extends Controller{


   //Fetch all data
   public function all(){
    $table = TableRegistry::getTableLocator()->get('film');
    $query = $table
            ->find();
    $result = $query->toArray();

    $response = $result;

    $filmCategory = new FilmCategory();

    for($j = 0; $j < count($result); $j++){
      $category = $filmCategory->getCategory($result[$j]['film_id']);
      $response[$j]['category'] = $category[0]['category_id'];
    }

    return $response;
  }




}
