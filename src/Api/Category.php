<?php
namespace App\Api;

use Cake\Controller\Controller;
use App\Controller\UsersController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\Datasource\EntityInterface;
use Cake\Core\Configure;




Class Category extends Controller{


   //Fetch all data
   public function all(){
    $table = TableRegistry::getTableLocator()->get('category');
    $query = $table
            ->find();
    $result = $query->toArray();
    return $result;
  }
}
