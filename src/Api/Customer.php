<?php
namespace App\Api;

use Cake\Controller\Controller;
use App\Controller\UsersController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\Datasource\EntityInterface;
use Cake\Core\Configure;




Class Customer extends Controller{


  public function makeInActive($id){
    $table = TableRegistry::getTableLocator()->get('customer');
    $customer = $table->get($id);
    if($customer != null){
      $customer->active = 0;
      $table->save($customer);
    }
   return $this->all();
  }

  public function makeActive($id){
    $table = TableRegistry::getTableLocator()->get('customer');
    $customer = $table->get($id);
    if($customer != null){
      $customer->active = 1;
      $table->save($customer);
    }
   return $this->all();
  }


   //Fetch all data
   public function all(){
    $table = TableRegistry::getTableLocator()->get('customer');
    $query = $table
            ->find();
    $result = $query->toArray();

    $response = $result;

    $rental = new Rental();

    for($j = 0; $j < count($result); $j++){
      $rentals = $rental->customer($result[$j]['customer_id']);
      $response[$j]['rentals'] = count($rentals);
    }

    return $response;
  }

  //Fetch by ID
  public function get($id){
    $table = TableRegistry::getTableLocator()->get('customers');
    $query = $table
            ->find()
            ->where(['id' => $id]);
    $result = $query->toArray();
    return $result;
  }

 







}
